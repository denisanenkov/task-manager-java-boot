package ru.anenkov.tm;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TaskManagerApplication {

    public static void main(String[] args) throws Exception {
        SpringApplication.run(TaskManagerApplication.class, args);
    }

}
