package ru.anenkov.tm.exception.empty;

import ru.anenkov.tm.exception.AbstractException;

public class EmptySignatureSessionException extends AbstractException {

    public EmptySignatureSessionException() {
        super("Error! Session signature is empty!");
    }

}
