package ru.anenkov.tm.api.endpoint.soap;

import org.jetbrains.annotations.Nullable;
import ru.anenkov.tm.dto.ProjectDTO;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import java.util.List;

public interface IProjectEndpoint {

    @WebMethod
    long count();

    @WebMethod
    void removeAllProjects();

    @WebMethod
    List<ProjectDTO> getList();

    @WebMethod
    void removeOneById(@WebParam(name = "id") @Nullable String id);

    @WebMethod
    void addProject(@WebParam(name = "project") ProjectDTO project);

    @WebMethod
    ProjectDTO findOneByIdEntity(@WebParam(name = "id") @Nullable String id);

    @WebMethod
    ProjectDTO findProjectByUserIdAndName(@Nullable @WebParam(name = "name") String name);

    @WebMethod
    ProjectDTO findProjectByUserIdAndId(@WebParam(name = "userId") @Nullable String userId, @WebParam(name = "id") @Nullable String id);

}
