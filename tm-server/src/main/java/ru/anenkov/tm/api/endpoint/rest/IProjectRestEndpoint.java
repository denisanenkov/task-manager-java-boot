package ru.anenkov.tm.api.endpoint.rest;

import org.springframework.web.bind.annotation.*;
import org.jetbrains.annotations.Nullable;
import org.springframework.http.MediaType;
import ru.anenkov.tm.dto.ProjectDTO;

import java.util.List;

public interface IProjectRestEndpoint {

    @DeleteMapping(value = "/projects")
    void removeAllProjects();

    @DeleteMapping(value = "/projects/{id}")
    String removeOneById(@Nullable @PathVariable String id);

    @PostMapping(value = "/projects")
    ProjectDTO add(@Nullable @RequestBody ProjectDTO project);

    @PutMapping(value = "/projects")
    ProjectDTO update(@Nullable @RequestBody ProjectDTO project);

    @Nullable
    @GetMapping(value = "/projects", produces = MediaType.APPLICATION_JSON_VALUE)
    List<ProjectDTO> getListByUserId();

    @GetMapping(value = "/projects/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    @Nullable ProjectDTO findOneByIdEntity(@Nullable @PathVariable("id") String id);

    @GetMapping(value = "/projects/count", produces = MediaType.APPLICATION_JSON_VALUE)
    long count();

}
