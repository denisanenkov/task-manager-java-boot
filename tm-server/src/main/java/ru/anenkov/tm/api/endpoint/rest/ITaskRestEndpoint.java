package ru.anenkov.tm.api.endpoint.rest;

import org.springframework.web.bind.annotation.*;
import org.jetbrains.annotations.Nullable;
import org.springframework.http.MediaType;
import ru.anenkov.tm.dto.TaskDTO;

import java.util.List;

public interface ITaskRestEndpoint {

    @PostMapping(value = "/tasks")
    TaskDTO addTask(@RequestBody @Nullable TaskDTO task);

    @GetMapping(value = "/{projectId}/tasks/count")
    long count(@PathVariable @Nullable String projectId);

    @PutMapping(value = "/tasks")
    TaskDTO updateTask(@RequestBody @Nullable TaskDTO task);

    @DeleteMapping(value = "/{projectId}/tasks")
    void deleteAllTasks(@PathVariable @Nullable String projectId);

    @GetMapping(value = "/{projectId}/tasks", produces = MediaType.APPLICATION_JSON_VALUE)
    List<TaskDTO> tasks(@PathVariable @Nullable String projectId);

    @GetMapping(value = "/{projectId}/tasks/{id}")
    TaskDTO getTask(@PathVariable @Nullable String projectId, @PathVariable @Nullable String id);

    @DeleteMapping(value = "/{projectId}/tasks/{id}")
    String deleteTask(@PathVariable @Nullable String projectId, @PathVariable @Nullable String id);

}
