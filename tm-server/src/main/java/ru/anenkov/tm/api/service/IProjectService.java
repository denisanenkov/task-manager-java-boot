package ru.anenkov.tm.api.service;

import org.springframework.transaction.annotation.Transactional;
import org.jetbrains.annotations.Nullable;
import ru.anenkov.tm.dto.ProjectDTO;
import ru.anenkov.tm.model.Project;
import lombok.SneakyThrows;

import java.util.List;
import java.util.Optional;

public interface IProjectService {

    @SneakyThrows
    @Transactional
    void add(@Nullable Project project);

    @SneakyThrows
    ProjectDTO toProjectDTO(Project project);

    @SneakyThrows
    @Transactional
    void addDTO(@Nullable ProjectDTO project);

    @SneakyThrows
    @Transactional(readOnly = true)
    long countByUserId(@Nullable String userId);

    @SneakyThrows
    @Transactional
    void removeAllByUserId(@Nullable String userId);

    @Nullable
    @SneakyThrows
    @Transactional(readOnly = true)
    List<Project> findAllByUserId(@Nullable String userId);

    @SneakyThrows
    Project toProject(@Nullable Optional<Project> projectDTO);

    @SneakyThrows
    @Transactional
    void removeProjectByUserIdAndId(@Nullable String userId, @Nullable String id);

    @Nullable
    @SneakyThrows
    @Transactional(readOnly = true)
    Project findProjectByIdAndUserId(@Nullable String userId, @Nullable String id);

    @SneakyThrows
    List<Project> toProjectList(@Nullable List<Optional<Project>> projectOptDtoList);

    @SneakyThrows
    @Transactional(readOnly = true)
    Project findProjectByUserIdAndName(@Nullable String userId, @Nullable String name);

}
