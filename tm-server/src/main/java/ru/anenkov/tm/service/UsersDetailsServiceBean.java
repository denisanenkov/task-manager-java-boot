package ru.anenkov.tm.service;

import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.anenkov.tm.constant.CredentialsConstant;
import ru.anenkov.tm.dto.CustomUser;
import ru.anenkov.tm.enumerated.RoleType;
import ru.anenkov.tm.exception.empty.EmptyLoginException;
import ru.anenkov.tm.exception.empty.EmptyPasswordException;
import ru.anenkov.tm.model.Role;
import ru.anenkov.tm.model.User;
import ru.anenkov.tm.repository.UserRepository;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@Service("userDetailsService")
public class UsersDetailsServiceBean implements UserDetailsService {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Nullable
    public User findByLogin(@Nullable final String login) {
        return userRepository.findByLogin(login);
    }

    @Override
    @Nullable
    public UserDetails loadUserByUsername
            (@Nullable final String username) throws UsernameNotFoundException {
        if (username == null || username.isEmpty()) throw new EmptyLoginException();
        @Nullable final User user = findByLogin(username);
        if (user == null) throw new UsernameNotFoundException(username);
        final List<Role> userRoles = user.getRoles();
        final List<String> roles = new ArrayList<>();
        for (final Role role : userRoles) roles.add(role.toString());
        return new CustomUser(org.springframework.security.core.userdetails.User
                .withUsername(user.getLogin())
                .password(user.getPasswordHash())
                .roles(roles.toArray(new String[]{}))
                .build()).withUserId(user.getId());
    }

    @PostConstruct
    public void initUsers() {
        if (userRepository.count() > 0) return;
        create(CredentialsConstant.FIRST_USER_LOGIN, CredentialsConstant.FIRST_USER_PASSWORD, RoleType.ADMINISTRATOR);
        create(CredentialsConstant.SECOND_USER_LOGIN, CredentialsConstant.SECOND_USER_PASSWORD, RoleType.USER);
        create(CredentialsConstant.THIRD_USER_LOGIN, CredentialsConstant.THIRD_USER_PASSWORD, RoleType.USER);
        create(CredentialsConstant.FOURTH_USER_LOGIN, CredentialsConstant.FOURTH_USER_PASSWORD, RoleType.USER);
    }

    @Transactional
    public void create(
            @Nullable final String login,
            @Nullable final String password,
            @Nullable final RoleType roleType
    ) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        if (password == null || password.isEmpty()) throw new EmptyPasswordException();
        @Nullable final User user = new User();
        user.setLogin(login);
        user.setPasswordHash(passwordEncoder.encode(password));
        @Nullable final Role role = new Role();
        role.setUser(user);
        role.setRoleType(roleType);
        user.setRoles(Collections.singletonList(role));
        userRepository.save(user);
    }

    private void initializationUser(
            @Nullable final String login,
            @Nullable final String password,
            @Nullable final RoleType roleType
    ) {
        final User user = findByLogin(login);
        if (user != null) return;
        create(login, password, roleType);
    }

}
