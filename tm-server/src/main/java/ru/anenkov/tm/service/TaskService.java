package ru.anenkov.tm.service;

import lombok.SneakyThrows;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.anenkov.tm.api.service.ITaskService;
import ru.anenkov.tm.dto.TaskDTO;
import ru.anenkov.tm.exception.empty.EmptyEntityException;
import ru.anenkov.tm.exception.empty.EmptyIdException;
import ru.anenkov.tm.model.Task;
import ru.anenkov.tm.repository.TaskDtoRepository;
import ru.anenkov.tm.repository.TaskRepository;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class TaskService implements ITaskService {

    @Nullable
    @Autowired
    private TaskRepository taskRepository;

    @Nullable
    @Autowired
    private TaskDtoRepository taskDtoRepository;

    @Override
    @SneakyThrows
    public Task toTask(@Nullable final Optional<Task> taskDTO) {
        if (taskDTO == null) throw new EmptyEntityException();
        @Nullable Task newTask = new Task();
        newTask.setId(taskDTO.get().getId());
        newTask.setName(taskDTO.get().getName());
        newTask.setDescription(taskDTO.get().getDescription());
        newTask.setDateBegin(taskDTO.get().getDateBegin());
        newTask.setDateFinish(taskDTO.get().getDateFinish());
        newTask.setStatus(taskDTO.get().getStatus());
        newTask.setProject(taskDTO.get().getProject());
        return newTask;
    }

    @Override
    @SneakyThrows
    public List<Task> toTaskList(@Nullable final List<Optional<Task>> taskOptDtoList) {
        if (taskOptDtoList == null || taskOptDtoList.isEmpty()) throw new EmptyEntityException();
        @Nullable List<Task> taskList = new ArrayList<>();
        for (Optional<Task> task : taskOptDtoList) {
            taskList.add(toTask(task));
        }
        return taskList;
    }

    @Override
    @SneakyThrows
    @Transactional
    public void removeAllByUserIdAndProjectId(
            @Nullable final String userId,
            @Nullable final String projectId
    ) {
        if (userId == null || userId.isEmpty()) throw new EmptyIdException();
        if (projectId == null || projectId.isEmpty()) throw new EmptyIdException();
        taskRepository.removeAllByUserIdAndProjectId(userId, projectId);
    }

    @Override
    @SneakyThrows
    @Transactional(readOnly = true)
    public List<Task> findAllByUserId(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) throw new EmptyIdException();
        return taskRepository.findAllByUserId(userId);
    }

    @Override
    @SneakyThrows
    @Transactional(readOnly = true)
    public List<Task> findAllByUserIdAndProjectId(
            @Nullable final String userId,
            @Nullable final String projectId
    ) {
        if (userId == null || userId.isEmpty()) throw new EmptyIdException();
        if (projectId == null || projectId.isEmpty()) throw new EmptyIdException();
        return taskRepository.findAllByUserIdAndProjectId(userId, projectId);
    }

    @Override
    @SneakyThrows
    @Transactional(readOnly = true)
    public Task findTaskByUserIdAndProjectIdAndId(
            @Nullable final String userId,
            @Nullable final String projectId,
            @Nullable final String id
    ) {
        if (userId == null || userId.isEmpty()) throw new EmptyIdException();
        if (projectId == null || projectId.isEmpty()) throw new EmptyIdException();
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        return taskRepository.findTaskByUserIdAndProjectIdAndId(userId, projectId, id);
    }

    @Override
    @SneakyThrows
    @Transactional(readOnly = true)
    public Task findTaskByUserIdAndProjectIdAndName(
            @Nullable final String userId,
            @Nullable final String projectId,
            @Nullable final String name
    ) {
        if (userId == null || userId.isEmpty()) throw new EmptyIdException();
        if (projectId == null || projectId.isEmpty()) throw new EmptyIdException();
        if (name == null || name.isEmpty()) throw new EmptyIdException();
        return taskRepository.findTaskByUserIdAndProjectIdAndName(userId, projectId, name);
    }

    @Override
    @SneakyThrows
    @Transactional
    public void removeTaskByIdAndUserId(
            @Nullable final String userId,
            @Nullable final String id
    ) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        if (userId == null || userId.isEmpty()) throw new EmptyEntityException();
        taskRepository.removeTaskByUserIdAndId(userId, id);
    }

    @Override
    @SneakyThrows
    @Transactional
    public void removeTaskByUserIdAndProjectIdAndId(
            @Nullable final String userId,
            @Nullable final String projectId,
            @Nullable final String id
    ) {
        if (userId == null || userId.isEmpty()) throw new EmptyIdException();
        if (projectId == null || projectId.isEmpty()) throw new EmptyIdException();
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        taskRepository.removeTaskByUserIdAndProjectIdAndId(userId, projectId, id);
    }

    @Override
    @SneakyThrows
    @Transactional
    public void addDTO(
            @Nullable final TaskDTO task
    ) {
        if (task == null) throw new EmptyEntityException();
        taskDtoRepository.save(task);
    }

    @Override
    @SneakyThrows
    @Transactional(readOnly = true)
    public long countByUserIdAndProjectId(
            @Nullable final String userId,
            @Nullable final String projectId
    ) {
        if (userId == null || userId.isEmpty()) throw new EmptyIdException();
        if (projectId == null || projectId.isEmpty()) throw new EmptyIdException();
        return taskRepository.countByUserIdAndProjectId(userId, projectId);
    }

    @Override
    @SneakyThrows
    @Transactional
    public void add(
            @Nullable final Task task
    ) {
        if (task == null) throw new EmptyEntityException();
        taskRepository.save(task);
    }

    @Override
    @SneakyThrows
    @Transactional
    public Task findTaskByUserIdAndId(
            @Nullable final String userId,
            @Nullable final String id
    ) {
        if (userId == null || userId.isEmpty()) throw new EmptyIdException();
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        return taskRepository.findTaskByUserIdAndId(userId, id);
    }

    /**
     * no secure
     */

    @Nullable
    @SneakyThrows
    @Transactional(readOnly = true)
    public Task findProjectId(@Nullable final String id) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        return taskRepository.findById(id).orElse(null);
    }

    @Nullable
    @SneakyThrows
    @Transactional(readOnly = true)
    public List<Task> findAllProjects() {
        return taskRepository.findAll();
    }

    @SneakyThrows
    @Transactional
    public void deleteById(@Nullable final String id) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        taskRepository.deleteById(id);
    }

    @SneakyThrows
    @Transactional
    public void deleteAll() {
        taskRepository.deleteAll();
    }

    @Nullable
    @SneakyThrows
    @Transactional
    public Task save(@Nullable final Task task) {
        return taskRepository.save(task);
    }

    @SneakyThrows
    @Transactional(readOnly = true)
    public long count() {
        return taskRepository.count();
    }

}
