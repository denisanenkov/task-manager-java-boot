package ru.anenkov.tm.controller;

import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.security.core.Authentication;
import org.springframework.validation.BindingResult;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.stereotype.Controller;
import ru.anenkov.tm.service.ProjectService;
import org.jetbrains.annotations.Nullable;
import ru.anenkov.tm.enumerated.Status;
import ru.anenkov.tm.dto.CustomUser;
import org.springframework.ui.Model;
import ru.anenkov.tm.dto.ProjectDTO;

@Controller
public class ProjectController {

    @Autowired
    private ProjectService projectService;

    @ModelAttribute("status")
    public Status[] getStatuses() {
        return Status.values();
    }

    @GetMapping("/projects")
    public ModelAndView index(
            @AuthenticationPrincipal @Nullable final CustomUser user,
            @Nullable Model model) {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        String name = authentication.getName();
        model.addAttribute("name", name);
        return new ModelAndView("project-list", "projects", projectService.findAllByUserId(user.getUserId()));
    }

    @GetMapping("/project/create")
    public String create(
            @AuthenticationPrincipal @Nullable final CustomUser user
    ) {
        final ProjectDTO project = new ProjectDTO("new Project" + System.currentTimeMillis());
        project.setUserId(user.getUserId());
        projectService.addDTO(project);
        return "redirect:/projects";
    }

    @GetMapping("/project/delete/{id}")
    public String delete(
            @AuthenticationPrincipal @Nullable final CustomUser user,
            @PathVariable("id") @Nullable final String id
    ) {
        projectService.removeProjectByUserIdAndId(user.getUserId(), id);
        return "redirect:/projects";
    }

    @PostMapping("/project/edit/{id}")
    public String edit(
            @AuthenticationPrincipal @Nullable final CustomUser user,
            @ModelAttribute("project") @Nullable ProjectDTO project,
            BindingResult result
    ) {
        project.setUserId(user.getUserId());
        projectService.addDTO(project);
        return "redirect:/projects";
    }

    @GetMapping("/project/edit/{id}")
    public ModelAndView edit(
            @AuthenticationPrincipal @Nullable final CustomUser user,
            @PathVariable("id") @Nullable final String id
    ) {
        ProjectDTO project = ProjectDTO.toProjectDTO
                (projectService.findProjectByIdAndUserId(user.getUserId(), id));
        return new ModelAndView("project-edit", "project", project);
    }

}
