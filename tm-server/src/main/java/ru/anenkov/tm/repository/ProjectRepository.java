package ru.anenkov.tm.repository;

import org.jetbrains.annotations.Nullable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;
import ru.anenkov.tm.model.Project;

import java.util.List;

public interface ProjectRepository extends JpaRepository<Project, String> {

    @Transactional
    void removeAllByUserId(@Nullable @Param("userId") String userId);

    @Nullable
    @Transactional(readOnly = true)
    List<Project> findAllByUserId(@Nullable @Param("userId") String userId);

    @Nullable
    @Transactional(readOnly = true)
    Project findProjectByUserIdAndId(
            @Nullable @Param("id") String userId,
            @Nullable @Param("userId") String id);

    @Nullable
    @Transactional
    Project findProjectByUserIdAndName(
            @Nullable @Param("userId") String userId,
            @Nullable @Param("name") String name);

    @Transactional
    void removeProjectsByUserIdAndId(
            @Nullable @Param("id") String userId,
            @Nullable @Param("userId") String id);

    @Transactional(readOnly = true)
    long countByUserId(@Nullable @Param("userId") String userId);

}
