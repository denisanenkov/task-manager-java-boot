package ru.anenkov.tm.repository;

import org.jetbrains.annotations.Nullable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;
import ru.anenkov.tm.model.Task;

import java.util.List;

public interface TaskRepository extends JpaRepository<Task, String> {

    @Nullable
    @Transactional(readOnly = true)
    List<Task> findAllByUserIdAndProjectId(
            @Nullable @Param("userId") String userId,
            @Nullable @Param("projectId") String projectId);

    @Nullable
    @Transactional(readOnly = true)
    Task findTaskByUserIdAndProjectIdAndId(
            @Nullable @Param("userId") String userId,
            @Nullable @Param("projectId") String projectId,
            @Nullable @Param("id") String id);

    @Nullable
    @Transactional(readOnly = true)
    Task findTaskByUserIdAndProjectIdAndName(
            @Nullable @Param("userId") String userId,
            @Nullable @Param("projectId") String projectId,
            @Nullable @Param("name") String name);

    @Transactional(readOnly = true)
    long countByUserIdAndProjectId(
            @Nullable @Param("userId") String userId,
            @Nullable @Param("projectId") String projectId);

    @Transactional
    void removeTaskByUserIdAndProjectIdAndId(
            @Nullable @Param("userId") String userId,
            @Nullable @Param("projectId") String projectId,
            @Nullable @Param("id") String id);

    @Transactional
    void removeAllByUserIdAndProjectId(
            @Nullable @Param("userId") String userId,
            @Nullable @Param("projectId") String projectId);

    @Transactional
    void removeTaskByUserIdAndId(
            @Nullable @Param("id") String userId,
            @Nullable @Param("userId") String id);

    @Nullable
    @Transactional
    List<Task> findAllByUserId(
            @Nullable @Param("userId") String userId);

    @Nullable
    @Transactional
    Task findTaskByUserIdAndId(
            @Nullable @Param("userId") String userId,
            @Nullable @Param("id") String id);

}
