<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="fo" uri="http://www.springframework.org/tags/form" %>
<jsp:include page="../include/_header.jsp"/>
<h1>PROJECT EDIT</h1>
<form:form action="/project/edit/${project.id}/" method="post" modelAttribute="project">
    <input type="hidden" name="id" value="${project.id}">

    <p>
    <div>NAME:</div>
    <div>
        <form:input type="text" path="name"/>
    </div>
    </p>

    <p>
    <div>DESCRIPTION:</div>
    <div>
        <form:input type="text" path="description"/>
    </div>
    </p>

    <p>
    <div>STATUS:</div>
    <div>
        <form:select path="status">
            <form:options items="${status}"/>
        </form:select>
    </div>
    </p>

    <p>
    <div style="margin-bottom: 5px;">DATE BEGIN</div>
    <div>
        <form:input type="date" path="dateBegin"/>
    </div>
    </p>

    <p>
    <div style="margin-bottom: 5px;">DATE FINISH</div>
    <div>
        <form:input type="date" path="dateFinish"/>
    </div>
    </p>

    <button type="submit">SAVE PROJECT</button>
</form:form>
<jsp:include page="../include/_footer.jsp"/>