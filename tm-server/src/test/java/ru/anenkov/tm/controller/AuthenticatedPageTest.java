package ru.anenkov.tm.controller;

import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.security.test.context.support.WithUserDetails;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.web.servlet.MockMvc;
import ru.anenkov.tm.TaskManagerApplication;
import org.junit.runner.RunWith;
import org.junit.Test;

import static org.springframework.security.test.web.servlet.response.SecurityMockMvcResultMatchers.authenticated;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.assertj.core.api.Assertions.assertThat;

@WebAppConfiguration
@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = {TaskManagerApplication.class})
@AutoConfigureMockMvc
@WithUserDetails("forTests")
public class AuthenticatedPageTest {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    AuthController authController;

    @Test
    public void pageLoginViewTest() throws Exception {
        assertThat(authController).isNotNull();
        this.mockMvc.perform(MockMvcRequestBuilders.get("/"))
                .andDo(print())
                .andExpect(status().is2xxSuccessful())
                .andExpect(authenticated());
    }

    @Test
    public void pageTestsViewTest() throws Exception {
        assertThat(authController).isNotNull();
        this.mockMvc.perform(MockMvcRequestBuilders.get("/tasks"))
                .andDo(print())
                .andExpect(status().is2xxSuccessful())
                .andExpect(authenticated());
    }

    @Test
    public void pageProjectsViewTest() throws Exception {
        assertThat(authController).isNotNull();
        this.mockMvc.perform(MockMvcRequestBuilders.get("/projects"))
                .andDo(print())
                .andExpect(status().is2xxSuccessful())
                .andExpect(authenticated());
    }

    @Test
    public void pageTasksRestFreeViewTest() throws Exception {
        assertThat(authController).isNotNull();
        this.mockMvc.perform(MockMvcRequestBuilders.get("/api/free/tasks"))
                .andDo(print())
                .andExpect(status().is2xxSuccessful())
                .andExpect(authenticated());
    }

    @Test
    public void pageProjectsRestFreeViewTest() throws Exception {
        assertThat(authController).isNotNull();
        this.mockMvc.perform(MockMvcRequestBuilders.get("/api/free/projects"))
                .andDo(print())
                .andExpect(status().is2xxSuccessful())
                .andExpect(authenticated());
    }

}
