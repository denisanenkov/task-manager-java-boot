package ru.anenkov.tm.controller;

import org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestBuilders;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.web.client.RestTemplate;
import ru.anenkov.tm.TaskManagerApplication;
import ru.anenkov.tm.model.User;
import org.junit.runner.RunWith;
import org.junit.Assert;
import org.junit.Test;

import static org.springframework.security.test.web.servlet.response.SecurityMockMvcResultMatchers.unauthenticated;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.redirectedUrl;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.assertj.core.api.Assertions.assertThat;

@WebAppConfiguration
@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = {TaskManagerApplication.class})
@AutoConfigureMockMvc
public class LoginTest {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    AuthController authController;

    @Test
    public void loginCorrectDataTest() {
        Assert.assertTrue(authController.login
                ("forTests", "forTests"));
    }

    @Test
    public void loginIncorrectTest() {
        Assert.assertFalse(authController.login
                ("error", "error"));
    }

    @Test
    public void pageLoginViewTest() throws Exception {
        assertThat(authController).isNotNull();
        this.mockMvc.perform(MockMvcRequestBuilders.get("/login"))
                .andDo(print())
                .andExpect(MockMvcResultMatchers.status().isOk());
    }

    @Test
    public void pageLoginViewIntegrationTest() {
        final RestTemplate restTemplate = new RestTemplate();
        final String result = restTemplate.getForObject("http://localhost:8080/login", String.class);
        Assert.assertTrue(result.contains("Please sign in"));
        System.out.println(result);
    }

    @Test
    public void nonAuthTaskPageTest() throws Exception {
        this.mockMvc.perform(MockMvcRequestBuilders.get("/tasks"))
                .andDo(print())
                .andExpect(status().is3xxRedirection())
                .andExpect(redirectedUrl("http://localhost/login"));
    }

    @Test
    public void nonAuthProjectsPageTest() throws Exception {
        this.mockMvc.perform(MockMvcRequestBuilders.get("/projects"))
                .andDo(print())
                .andExpect(status().is3xxRedirection())
                .andExpect(redirectedUrl("http://localhost/login"));
    }

    @Test
    public void nonAuthMainPageTest() throws Exception {
        this.mockMvc.perform(MockMvcRequestBuilders.get("/"))
                .andDo(print())
                .andExpect(status().is2xxSuccessful());
    }

    @Test
    public void correctCredentialsTest() throws Exception {
        this.mockMvc.perform(SecurityMockMvcRequestBuilders.
                formLogin().user("forTests").password("forTests"))
                .andDo(print())
                .andExpect(status().is3xxRedirection())
                .andExpect(redirectedUrl("/"));
    }

    @Test
    public void incorrectCredentialsTest() throws Exception {
        this.mockMvc.perform(SecurityMockMvcRequestBuilders.
                formLogin().user("error").password("error"))
                .andDo(print())
                .andExpect(status().is3xxRedirection())
                .andExpect(redirectedUrl("/login?error"));
    }

    @Test
    public void pageTasksRestViewTest() throws Exception {
        assertThat(authController).isNotNull();
        this.mockMvc.perform(MockMvcRequestBuilders.get("/api/free/tasks"))
                .andDo(print())
                .andExpect(status().is2xxSuccessful())
                .andExpect(unauthenticated());
    }

    @Test
    public void pageProjectsRestViewTest() throws Exception {
        assertThat(authController).isNotNull();
        this.mockMvc.perform(MockMvcRequestBuilders.get("/api/free/projects"))
                .andDo(print())
                .andExpect(status().is2xxSuccessful())
                .andExpect(unauthenticated());
    }

    @Test
    public void getProfileTest() {
        authController.login("forTests", "forTests");
        User user = null;
        Assert.assertNull(user);
        user = authController.profile();
        Assert.assertNotNull(user);
        Assert.assertNotNull(user.getId());
        Assert.assertNotNull(user.getLogin());
        Assert.assertNotNull(user.getPasswordHash());
        Assert.assertEquals(user.getLogin(), "forTests");
    }

}
