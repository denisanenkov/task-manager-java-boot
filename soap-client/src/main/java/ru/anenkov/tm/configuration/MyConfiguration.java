package ru.anenkov.tm.configuration;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.client.RestTemplate;
import ru.anenkov.tm.endpoint.soap.*;

@Configuration
@ComponentScan("ru.anenkov.tm")
public class MyConfiguration {

    @Bean
    public RestTemplate restTemplate() {
        return new RestTemplate();
    }

    @Bean
    public AuthEndpointService authEndpointService() {
        return new AuthEndpointService();
    }

    @Bean
    public AuthEndpoint authEndpoint(
            @Autowired final AuthEndpointService authEndpointService
    ) {
        return authEndpointService.getAuthEndpointPort();
    }

    @Bean
    public ProjectEndpointService projectEndpointService() {
        return new ProjectEndpointService();
    }

    @Bean
    public ProjectEndpoint projectEndpoint(
            @Autowired final ProjectEndpointService projectEndpointService
    ) {
        return projectEndpointService.getProjectEndpointPort();
    }

    @Bean
    public TaskEndpointService taskEndpointService() {
        return new TaskEndpointService();
    }

    @Bean
    public TaskEndpoint taskEndpoint(
            @Autowired final TaskEndpointService taskEndpointService
    ) {
        return taskEndpointService.getTaskEndpointPort();
    }

}