
package ru.anenkov.tm.endpoint.soap;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each
 * Java content interface and Java element interface
 * generated in the ru.anenkov.tm.endpoint.soap package.
 * &lt;p&gt;An ObjectFactory allows you to programatically
 * construct new instances of the Java representation
 * for XML content. The Java representation of XML
 * content can consist of schema derived interfaces
 * and classes representing the binding of schema
 * type definitions, element declarations and model
 * groups.  Factory methods for each of these are
 * provided in this class.
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _CountUsers_QNAME = new QName("http://soap.endpoint.tm.anenkov.ru/", "countUsers");
    private final static QName _CountUsersResponse_QNAME = new QName("http://soap.endpoint.tm.anenkov.ru/", "countUsersResponse");
    private final static QName _DeleteAllUsers_QNAME = new QName("http://soap.endpoint.tm.anenkov.ru/", "deleteAllUsers");
    private final static QName _DeleteAllUsersResponse_QNAME = new QName("http://soap.endpoint.tm.anenkov.ru/", "deleteAllUsersResponse");
    private final static QName _DeleteUser_QNAME = new QName("http://soap.endpoint.tm.anenkov.ru/", "deleteUser");
    private final static QName _DeleteUserById_QNAME = new QName("http://soap.endpoint.tm.anenkov.ru/", "deleteUserById");
    private final static QName _DeleteUserByIdResponse_QNAME = new QName("http://soap.endpoint.tm.anenkov.ru/", "deleteUserByIdResponse");
    private final static QName _DeleteUserByLogin_QNAME = new QName("http://soap.endpoint.tm.anenkov.ru/", "deleteUserByLogin");
    private final static QName _DeleteUserByLoginResponse_QNAME = new QName("http://soap.endpoint.tm.anenkov.ru/", "deleteUserByLoginResponse");
    private final static QName _DeleteUserResponse_QNAME = new QName("http://soap.endpoint.tm.anenkov.ru/", "deleteUserResponse");
    private final static QName _ExistsUserById_QNAME = new QName("http://soap.endpoint.tm.anenkov.ru/", "existsUserById");
    private final static QName _ExistsUserByIdResponse_QNAME = new QName("http://soap.endpoint.tm.anenkov.ru/", "existsUserByIdResponse");
    private final static QName _FindUserByFirstName_QNAME = new QName("http://soap.endpoint.tm.anenkov.ru/", "findUserByFirstName");
    private final static QName _FindUserByFirstNameResponse_QNAME = new QName("http://soap.endpoint.tm.anenkov.ru/", "findUserByFirstNameResponse");
    private final static QName _FindUserById_QNAME = new QName("http://soap.endpoint.tm.anenkov.ru/", "findUserById");
    private final static QName _FindUserByIdResponse_QNAME = new QName("http://soap.endpoint.tm.anenkov.ru/", "findUserByIdResponse");
    private final static QName _FindUserByLastName_QNAME = new QName("http://soap.endpoint.tm.anenkov.ru/", "findUserByLastName");
    private final static QName _FindUserByLastNameResponse_QNAME = new QName("http://soap.endpoint.tm.anenkov.ru/", "findUserByLastNameResponse");
    private final static QName _FindUserByLogin_QNAME = new QName("http://soap.endpoint.tm.anenkov.ru/", "findUserByLogin");
    private final static QName _FindUserByLoginResponse_QNAME = new QName("http://soap.endpoint.tm.anenkov.ru/", "findUserByLoginResponse");
    private final static QName _FindUserBySecondName_QNAME = new QName("http://soap.endpoint.tm.anenkov.ru/", "findUserBySecondName");
    private final static QName _FindUserBySecondNameResponse_QNAME = new QName("http://soap.endpoint.tm.anenkov.ru/", "findUserBySecondNameResponse");
    private final static QName _Login_QNAME = new QName("http://soap.endpoint.tm.anenkov.ru/", "login");
    private final static QName _LoginResponse_QNAME = new QName("http://soap.endpoint.tm.anenkov.ru/", "loginResponse");
    private final static QName _Logout_QNAME = new QName("http://soap.endpoint.tm.anenkov.ru/", "logout");
    private final static QName _LogoutResponse_QNAME = new QName("http://soap.endpoint.tm.anenkov.ru/", "logoutResponse");
    private final static QName _Profile_QNAME = new QName("http://soap.endpoint.tm.anenkov.ru/", "profile");
    private final static QName _ProfileResponse_QNAME = new QName("http://soap.endpoint.tm.anenkov.ru/", "profileResponse");
    private final static QName _SaveUser_QNAME = new QName("http://soap.endpoint.tm.anenkov.ru/", "saveUser");
    private final static QName _SaveUserResponse_QNAME = new QName("http://soap.endpoint.tm.anenkov.ru/", "saveUserResponse");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: ru.anenkov.tm.endpoint.soap
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link CountUsers }
     */
    public CountUsers createCountUsers() {
        return new CountUsers();
    }

    /**
     * Create an instance of {@link CountUsersResponse }
     */
    public CountUsersResponse createCountUsersResponse() {
        return new CountUsersResponse();
    }

    /**
     * Create an instance of {@link DeleteAllUsers }
     */
    public DeleteAllUsers createDeleteAllUsers() {
        return new DeleteAllUsers();
    }

    /**
     * Create an instance of {@link DeleteAllUsersResponse }
     */
    public DeleteAllUsersResponse createDeleteAllUsersResponse() {
        return new DeleteAllUsersResponse();
    }

    /**
     * Create an instance of {@link DeleteUser }
     */
    public DeleteUser createDeleteUser() {
        return new DeleteUser();
    }

    /**
     * Create an instance of {@link DeleteUserById }
     */
    public DeleteUserById createDeleteUserById() {
        return new DeleteUserById();
    }

    /**
     * Create an instance of {@link DeleteUserByIdResponse }
     */
    public DeleteUserByIdResponse createDeleteUserByIdResponse() {
        return new DeleteUserByIdResponse();
    }

    /**
     * Create an instance of {@link DeleteUserByLogin }
     */
    public DeleteUserByLogin createDeleteUserByLogin() {
        return new DeleteUserByLogin();
    }

    /**
     * Create an instance of {@link DeleteUserByLoginResponse }
     */
    public DeleteUserByLoginResponse createDeleteUserByLoginResponse() {
        return new DeleteUserByLoginResponse();
    }

    /**
     * Create an instance of {@link DeleteUserResponse }
     */
    public DeleteUserResponse createDeleteUserResponse() {
        return new DeleteUserResponse();
    }

    /**
     * Create an instance of {@link ExistsUserById }
     */
    public ExistsUserById createExistsUserById() {
        return new ExistsUserById();
    }

    /**
     * Create an instance of {@link ExistsUserByIdResponse }
     */
    public ExistsUserByIdResponse createExistsUserByIdResponse() {
        return new ExistsUserByIdResponse();
    }

    /**
     * Create an instance of {@link FindUserByFirstName }
     */
    public FindUserByFirstName createFindUserByFirstName() {
        return new FindUserByFirstName();
    }

    /**
     * Create an instance of {@link FindUserByFirstNameResponse }
     */
    public FindUserByFirstNameResponse createFindUserByFirstNameResponse() {
        return new FindUserByFirstNameResponse();
    }

    /**
     * Create an instance of {@link FindUserById }
     */
    public FindUserById createFindUserById() {
        return new FindUserById();
    }

    /**
     * Create an instance of {@link FindUserByIdResponse }
     */
    public FindUserByIdResponse createFindUserByIdResponse() {
        return new FindUserByIdResponse();
    }

    /**
     * Create an instance of {@link FindUserByLastName }
     */
    public FindUserByLastName createFindUserByLastName() {
        return new FindUserByLastName();
    }

    /**
     * Create an instance of {@link FindUserByLastNameResponse }
     */
    public FindUserByLastNameResponse createFindUserByLastNameResponse() {
        return new FindUserByLastNameResponse();
    }

    /**
     * Create an instance of {@link FindUserByLogin }
     */
    public FindUserByLogin createFindUserByLogin() {
        return new FindUserByLogin();
    }

    /**
     * Create an instance of {@link FindUserByLoginResponse }
     */
    public FindUserByLoginResponse createFindUserByLoginResponse() {
        return new FindUserByLoginResponse();
    }

    /**
     * Create an instance of {@link FindUserBySecondName }
     */
    public FindUserBySecondName createFindUserBySecondName() {
        return new FindUserBySecondName();
    }

    /**
     * Create an instance of {@link FindUserBySecondNameResponse }
     */
    public FindUserBySecondNameResponse createFindUserBySecondNameResponse() {
        return new FindUserBySecondNameResponse();
    }

    /**
     * Create an instance of {@link Login }
     */
    public Login createLogin() {
        return new Login();
    }

    /**
     * Create an instance of {@link LoginResponse }
     */
    public LoginResponse createLoginResponse() {
        return new LoginResponse();
    }

    /**
     * Create an instance of {@link Logout }
     */
    public Logout createLogout() {
        return new Logout();
    }

    /**
     * Create an instance of {@link LogoutResponse }
     */
    public LogoutResponse createLogoutResponse() {
        return new LogoutResponse();
    }

    /**
     * Create an instance of {@link Profile }
     */
    public Profile createProfile() {
        return new Profile();
    }

    /**
     * Create an instance of {@link ProfileResponse }
     */
    public ProfileResponse createProfileResponse() {
        return new ProfileResponse();
    }

    /**
     * Create an instance of {@link SaveUser }
     */
    public SaveUser createSaveUser() {
        return new SaveUser();
    }

    /**
     * Create an instance of {@link SaveUserResponse }
     */
    public SaveUserResponse createSaveUserResponse() {
        return new SaveUserResponse();
    }

    /**
     * Create an instance of {@link User }
     */
    public User createUser() {
        return new User();
    }

    /**
     * Create an instance of {@link Role }
     */
    public Role createRole() {
        return new Role();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CountUsers }{@code >}
     *
     * @param value Java instance representing xml element's value.
     * @return the new instance of {@link JAXBElement }{@code <}{@link CountUsers }{@code >}
     */
    @XmlElementDecl(namespace = "http://soap.endpoint.tm.anenkov.ru/", name = "countUsers")
    public JAXBElement<CountUsers> createCountUsers(CountUsers value) {
        return new JAXBElement<CountUsers>(_CountUsers_QNAME, CountUsers.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CountUsersResponse }{@code >}
     *
     * @param value Java instance representing xml element's value.
     * @return the new instance of {@link JAXBElement }{@code <}{@link CountUsersResponse }{@code >}
     */
    @XmlElementDecl(namespace = "http://soap.endpoint.tm.anenkov.ru/", name = "countUsersResponse")
    public JAXBElement<CountUsersResponse> createCountUsersResponse(CountUsersResponse value) {
        return new JAXBElement<CountUsersResponse>(_CountUsersResponse_QNAME, CountUsersResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DeleteAllUsers }{@code >}
     *
     * @param value Java instance representing xml element's value.
     * @return the new instance of {@link JAXBElement }{@code <}{@link DeleteAllUsers }{@code >}
     */
    @XmlElementDecl(namespace = "http://soap.endpoint.tm.anenkov.ru/", name = "deleteAllUsers")
    public JAXBElement<DeleteAllUsers> createDeleteAllUsers(DeleteAllUsers value) {
        return new JAXBElement<DeleteAllUsers>(_DeleteAllUsers_QNAME, DeleteAllUsers.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DeleteAllUsersResponse }{@code >}
     *
     * @param value Java instance representing xml element's value.
     * @return the new instance of {@link JAXBElement }{@code <}{@link DeleteAllUsersResponse }{@code >}
     */
    @XmlElementDecl(namespace = "http://soap.endpoint.tm.anenkov.ru/", name = "deleteAllUsersResponse")
    public JAXBElement<DeleteAllUsersResponse> createDeleteAllUsersResponse(DeleteAllUsersResponse value) {
        return new JAXBElement<DeleteAllUsersResponse>(_DeleteAllUsersResponse_QNAME, DeleteAllUsersResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DeleteUser }{@code >}
     *
     * @param value Java instance representing xml element's value.
     * @return the new instance of {@link JAXBElement }{@code <}{@link DeleteUser }{@code >}
     */
    @XmlElementDecl(namespace = "http://soap.endpoint.tm.anenkov.ru/", name = "deleteUser")
    public JAXBElement<DeleteUser> createDeleteUser(DeleteUser value) {
        return new JAXBElement<DeleteUser>(_DeleteUser_QNAME, DeleteUser.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DeleteUserById }{@code >}
     *
     * @param value Java instance representing xml element's value.
     * @return the new instance of {@link JAXBElement }{@code <}{@link DeleteUserById }{@code >}
     */
    @XmlElementDecl(namespace = "http://soap.endpoint.tm.anenkov.ru/", name = "deleteUserById")
    public JAXBElement<DeleteUserById> createDeleteUserById(DeleteUserById value) {
        return new JAXBElement<DeleteUserById>(_DeleteUserById_QNAME, DeleteUserById.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DeleteUserByIdResponse }{@code >}
     *
     * @param value Java instance representing xml element's value.
     * @return the new instance of {@link JAXBElement }{@code <}{@link DeleteUserByIdResponse }{@code >}
     */
    @XmlElementDecl(namespace = "http://soap.endpoint.tm.anenkov.ru/", name = "deleteUserByIdResponse")
    public JAXBElement<DeleteUserByIdResponse> createDeleteUserByIdResponse(DeleteUserByIdResponse value) {
        return new JAXBElement<DeleteUserByIdResponse>(_DeleteUserByIdResponse_QNAME, DeleteUserByIdResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DeleteUserByLogin }{@code >}
     *
     * @param value Java instance representing xml element's value.
     * @return the new instance of {@link JAXBElement }{@code <}{@link DeleteUserByLogin }{@code >}
     */
    @XmlElementDecl(namespace = "http://soap.endpoint.tm.anenkov.ru/", name = "deleteUserByLogin")
    public JAXBElement<DeleteUserByLogin> createDeleteUserByLogin(DeleteUserByLogin value) {
        return new JAXBElement<DeleteUserByLogin>(_DeleteUserByLogin_QNAME, DeleteUserByLogin.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DeleteUserByLoginResponse }{@code >}
     *
     * @param value Java instance representing xml element's value.
     * @return the new instance of {@link JAXBElement }{@code <}{@link DeleteUserByLoginResponse }{@code >}
     */
    @XmlElementDecl(namespace = "http://soap.endpoint.tm.anenkov.ru/", name = "deleteUserByLoginResponse")
    public JAXBElement<DeleteUserByLoginResponse> createDeleteUserByLoginResponse(DeleteUserByLoginResponse value) {
        return new JAXBElement<DeleteUserByLoginResponse>(_DeleteUserByLoginResponse_QNAME, DeleteUserByLoginResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DeleteUserResponse }{@code >}
     *
     * @param value Java instance representing xml element's value.
     * @return the new instance of {@link JAXBElement }{@code <}{@link DeleteUserResponse }{@code >}
     */
    @XmlElementDecl(namespace = "http://soap.endpoint.tm.anenkov.ru/", name = "deleteUserResponse")
    public JAXBElement<DeleteUserResponse> createDeleteUserResponse(DeleteUserResponse value) {
        return new JAXBElement<DeleteUserResponse>(_DeleteUserResponse_QNAME, DeleteUserResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ExistsUserById }{@code >}
     *
     * @param value Java instance representing xml element's value.
     * @return the new instance of {@link JAXBElement }{@code <}{@link ExistsUserById }{@code >}
     */
    @XmlElementDecl(namespace = "http://soap.endpoint.tm.anenkov.ru/", name = "existsUserById")
    public JAXBElement<ExistsUserById> createExistsUserById(ExistsUserById value) {
        return new JAXBElement<ExistsUserById>(_ExistsUserById_QNAME, ExistsUserById.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ExistsUserByIdResponse }{@code >}
     *
     * @param value Java instance representing xml element's value.
     * @return the new instance of {@link JAXBElement }{@code <}{@link ExistsUserByIdResponse }{@code >}
     */
    @XmlElementDecl(namespace = "http://soap.endpoint.tm.anenkov.ru/", name = "existsUserByIdResponse")
    public JAXBElement<ExistsUserByIdResponse> createExistsUserByIdResponse(ExistsUserByIdResponse value) {
        return new JAXBElement<ExistsUserByIdResponse>(_ExistsUserByIdResponse_QNAME, ExistsUserByIdResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FindUserByFirstName }{@code >}
     *
     * @param value Java instance representing xml element's value.
     * @return the new instance of {@link JAXBElement }{@code <}{@link FindUserByFirstName }{@code >}
     */
    @XmlElementDecl(namespace = "http://soap.endpoint.tm.anenkov.ru/", name = "findUserByFirstName")
    public JAXBElement<FindUserByFirstName> createFindUserByFirstName(FindUserByFirstName value) {
        return new JAXBElement<FindUserByFirstName>(_FindUserByFirstName_QNAME, FindUserByFirstName.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FindUserByFirstNameResponse }{@code >}
     *
     * @param value Java instance representing xml element's value.
     * @return the new instance of {@link JAXBElement }{@code <}{@link FindUserByFirstNameResponse }{@code >}
     */
    @XmlElementDecl(namespace = "http://soap.endpoint.tm.anenkov.ru/", name = "findUserByFirstNameResponse")
    public JAXBElement<FindUserByFirstNameResponse> createFindUserByFirstNameResponse(FindUserByFirstNameResponse value) {
        return new JAXBElement<FindUserByFirstNameResponse>(_FindUserByFirstNameResponse_QNAME, FindUserByFirstNameResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FindUserById }{@code >}
     *
     * @param value Java instance representing xml element's value.
     * @return the new instance of {@link JAXBElement }{@code <}{@link FindUserById }{@code >}
     */
    @XmlElementDecl(namespace = "http://soap.endpoint.tm.anenkov.ru/", name = "findUserById")
    public JAXBElement<FindUserById> createFindUserById(FindUserById value) {
        return new JAXBElement<FindUserById>(_FindUserById_QNAME, FindUserById.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FindUserByIdResponse }{@code >}
     *
     * @param value Java instance representing xml element's value.
     * @return the new instance of {@link JAXBElement }{@code <}{@link FindUserByIdResponse }{@code >}
     */
    @XmlElementDecl(namespace = "http://soap.endpoint.tm.anenkov.ru/", name = "findUserByIdResponse")
    public JAXBElement<FindUserByIdResponse> createFindUserByIdResponse(FindUserByIdResponse value) {
        return new JAXBElement<FindUserByIdResponse>(_FindUserByIdResponse_QNAME, FindUserByIdResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FindUserByLastName }{@code >}
     *
     * @param value Java instance representing xml element's value.
     * @return the new instance of {@link JAXBElement }{@code <}{@link FindUserByLastName }{@code >}
     */
    @XmlElementDecl(namespace = "http://soap.endpoint.tm.anenkov.ru/", name = "findUserByLastName")
    public JAXBElement<FindUserByLastName> createFindUserByLastName(FindUserByLastName value) {
        return new JAXBElement<FindUserByLastName>(_FindUserByLastName_QNAME, FindUserByLastName.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FindUserByLastNameResponse }{@code >}
     *
     * @param value Java instance representing xml element's value.
     * @return the new instance of {@link JAXBElement }{@code <}{@link FindUserByLastNameResponse }{@code >}
     */
    @XmlElementDecl(namespace = "http://soap.endpoint.tm.anenkov.ru/", name = "findUserByLastNameResponse")
    public JAXBElement<FindUserByLastNameResponse> createFindUserByLastNameResponse(FindUserByLastNameResponse value) {
        return new JAXBElement<FindUserByLastNameResponse>(_FindUserByLastNameResponse_QNAME, FindUserByLastNameResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FindUserByLogin }{@code >}
     *
     * @param value Java instance representing xml element's value.
     * @return the new instance of {@link JAXBElement }{@code <}{@link FindUserByLogin }{@code >}
     */
    @XmlElementDecl(namespace = "http://soap.endpoint.tm.anenkov.ru/", name = "findUserByLogin")
    public JAXBElement<FindUserByLogin> createFindUserByLogin(FindUserByLogin value) {
        return new JAXBElement<FindUserByLogin>(_FindUserByLogin_QNAME, FindUserByLogin.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FindUserByLoginResponse }{@code >}
     *
     * @param value Java instance representing xml element's value.
     * @return the new instance of {@link JAXBElement }{@code <}{@link FindUserByLoginResponse }{@code >}
     */
    @XmlElementDecl(namespace = "http://soap.endpoint.tm.anenkov.ru/", name = "findUserByLoginResponse")
    public JAXBElement<FindUserByLoginResponse> createFindUserByLoginResponse(FindUserByLoginResponse value) {
        return new JAXBElement<FindUserByLoginResponse>(_FindUserByLoginResponse_QNAME, FindUserByLoginResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FindUserBySecondName }{@code >}
     *
     * @param value Java instance representing xml element's value.
     * @return the new instance of {@link JAXBElement }{@code <}{@link FindUserBySecondName }{@code >}
     */
    @XmlElementDecl(namespace = "http://soap.endpoint.tm.anenkov.ru/", name = "findUserBySecondName")
    public JAXBElement<FindUserBySecondName> createFindUserBySecondName(FindUserBySecondName value) {
        return new JAXBElement<FindUserBySecondName>(_FindUserBySecondName_QNAME, FindUserBySecondName.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FindUserBySecondNameResponse }{@code >}
     *
     * @param value Java instance representing xml element's value.
     * @return the new instance of {@link JAXBElement }{@code <}{@link FindUserBySecondNameResponse }{@code >}
     */
    @XmlElementDecl(namespace = "http://soap.endpoint.tm.anenkov.ru/", name = "findUserBySecondNameResponse")
    public JAXBElement<FindUserBySecondNameResponse> createFindUserBySecondNameResponse(FindUserBySecondNameResponse value) {
        return new JAXBElement<FindUserBySecondNameResponse>(_FindUserBySecondNameResponse_QNAME, FindUserBySecondNameResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Login }{@code >}
     *
     * @param value Java instance representing xml element's value.
     * @return the new instance of {@link JAXBElement }{@code <}{@link Login }{@code >}
     */
    @XmlElementDecl(namespace = "http://soap.endpoint.tm.anenkov.ru/", name = "login")
    public JAXBElement<Login> createLogin(Login value) {
        return new JAXBElement<Login>(_Login_QNAME, Login.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link LoginResponse }{@code >}
     *
     * @param value Java instance representing xml element's value.
     * @return the new instance of {@link JAXBElement }{@code <}{@link LoginResponse }{@code >}
     */
    @XmlElementDecl(namespace = "http://soap.endpoint.tm.anenkov.ru/", name = "loginResponse")
    public JAXBElement<LoginResponse> createLoginResponse(LoginResponse value) {
        return new JAXBElement<LoginResponse>(_LoginResponse_QNAME, LoginResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Logout }{@code >}
     *
     * @param value Java instance representing xml element's value.
     * @return the new instance of {@link JAXBElement }{@code <}{@link Logout }{@code >}
     */
    @XmlElementDecl(namespace = "http://soap.endpoint.tm.anenkov.ru/", name = "logout")
    public JAXBElement<Logout> createLogout(Logout value) {
        return new JAXBElement<Logout>(_Logout_QNAME, Logout.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link LogoutResponse }{@code >}
     *
     * @param value Java instance representing xml element's value.
     * @return the new instance of {@link JAXBElement }{@code <}{@link LogoutResponse }{@code >}
     */
    @XmlElementDecl(namespace = "http://soap.endpoint.tm.anenkov.ru/", name = "logoutResponse")
    public JAXBElement<LogoutResponse> createLogoutResponse(LogoutResponse value) {
        return new JAXBElement<LogoutResponse>(_LogoutResponse_QNAME, LogoutResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Profile }{@code >}
     *
     * @param value Java instance representing xml element's value.
     * @return the new instance of {@link JAXBElement }{@code <}{@link Profile }{@code >}
     */
    @XmlElementDecl(namespace = "http://soap.endpoint.tm.anenkov.ru/", name = "profile")
    public JAXBElement<Profile> createProfile(Profile value) {
        return new JAXBElement<Profile>(_Profile_QNAME, Profile.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ProfileResponse }{@code >}
     *
     * @param value Java instance representing xml element's value.
     * @return the new instance of {@link JAXBElement }{@code <}{@link ProfileResponse }{@code >}
     */
    @XmlElementDecl(namespace = "http://soap.endpoint.tm.anenkov.ru/", name = "profileResponse")
    public JAXBElement<ProfileResponse> createProfileResponse(ProfileResponse value) {
        return new JAXBElement<ProfileResponse>(_ProfileResponse_QNAME, ProfileResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SaveUser }{@code >}
     *
     * @param value Java instance representing xml element's value.
     * @return the new instance of {@link JAXBElement }{@code <}{@link SaveUser }{@code >}
     */
    @XmlElementDecl(namespace = "http://soap.endpoint.tm.anenkov.ru/", name = "saveUser")
    public JAXBElement<SaveUser> createSaveUser(SaveUser value) {
        return new JAXBElement<SaveUser>(_SaveUser_QNAME, SaveUser.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SaveUserResponse }{@code >}
     *
     * @param value Java instance representing xml element's value.
     * @return the new instance of {@link JAXBElement }{@code <}{@link SaveUserResponse }{@code >}
     */
    @XmlElementDecl(namespace = "http://soap.endpoint.tm.anenkov.ru/", name = "saveUserResponse")
    public JAXBElement<SaveUserResponse> createSaveUserResponse(SaveUserResponse value) {
        return new JAXBElement<SaveUserResponse>(_SaveUserResponse_QNAME, SaveUserResponse.class, null, value);
    }

}
