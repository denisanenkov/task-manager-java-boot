
package ru.anenkov.tm.endpoint.soap;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * &lt;p&gt;Java class for addDTOTask complex type.
 * <p>
 * &lt;p&gt;The following schema fragment specifies the expected content contained within this class.
 * <p>
 * &lt;pre&gt;
 * &amp;lt;complexType name="addDTOTask"&amp;gt;
 * &amp;lt;complexContent&amp;gt;
 * &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 * &amp;lt;sequence&amp;gt;
 * &amp;lt;element name="task" type="{http://soap.endpoint.tm.anenkov.ru/}taskDTO" minOccurs="0"/&amp;gt;
 * &amp;lt;/sequence&amp;gt;
 * &amp;lt;/restriction&amp;gt;
 * &amp;lt;/complexContent&amp;gt;
 * &amp;lt;/complexType&amp;gt;
 * &lt;/pre&gt;
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "addDTOTask", propOrder = {
        "task"
})
public class AddDTOTask {

    protected TaskDTO task;

    /**
     * Gets the value of the task property.
     *
     * @return possible object is
     * {@link TaskDTO }
     */
    public TaskDTO getTask() {
        return task;
    }

    /**
     * Sets the value of the task property.
     *
     * @param value allowed object is
     *              {@link TaskDTO }
     */
    public void setTask(TaskDTO value) {
        this.task = value;
    }

}
