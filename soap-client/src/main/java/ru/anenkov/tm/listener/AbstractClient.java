package ru.anenkov.tm.listener;

import org.springframework.stereotype.Component;

import javax.xml.ws.BindingProvider;
import javax.xml.ws.handler.MessageContext;
import java.net.HttpCookie;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

@Component
public class AbstractClient {

    public static final String SLASH = "/";

    public static final String HEADER_COOKIE = "Cookie";

    public static final String HEADER_SET_COOKIE = "Set-Cookie";

    public static Map<String, Object> getRequestContext(Object port) {
        BindingProvider bindingProvider = getBindingProvider(port);
        if (bindingProvider == null) return null;
        return bindingProvider.getRequestContext();
    }

    public static Map<String, Object> getHttpRequestHeaders(Object port) {
        final Map<String, Object> requestContext = getRequestContext(port);
        if (requestContext == null) return null;
        return (Map<String, Object>) requestContext.get(MessageContext.HTTP_REQUEST_HEADERS);
    }

    public static void setListCookieRowRequest(Object port, final List<String> value) {
        if (getRequestContext(port) != null && getHttpRequestHeaders(port) == null)
            getRequestContext(port).put(MessageContext.HTTP_REQUEST_HEADERS, new LinkedHashMap<>());
        final Map<String, Object> httpRequestHeaders = getHttpRequestHeaders(port);
        if (httpRequestHeaders == null) return;
        httpRequestHeaders.put(HEADER_COOKIE, value);
    }

    public static void setMaintain(final Object port) {
        final BindingProvider bindingProvider = (BindingProvider) port;
        final Map<String, Object> map = bindingProvider.getRequestContext();
        map.put(BindingProvider.SESSION_MAINTAIN_PROPERTY, true);
    }

    @SuppressWarnings("unchecked")
    public static List<String> getListSetCookieRow(Object port) {
        final Map<String, Object> httpResponseHeaders = getHttpResponseHeaders(port);
        if (httpResponseHeaders == null) return null;
        return (List<String>) httpResponseHeaders.get(HEADER_SET_COOKIE);
    }

    public static List<HttpCookie> getListSetCookie(Object port) {
        final List<String> listSetCookieRow = getListSetCookieRow(port);
        if (listSetCookieRow == null) return null;
        final List<HttpCookie> result = new ArrayList<>();
        final List<String> listCookie = getListSetCookieRow(port);
        for (final String cookieRow : listCookie) {
            final List<HttpCookie> httpCookies = HttpCookie.parse(cookieRow);
            final HttpCookie httpCookie = httpCookies.get(0);
            result.add(httpCookie);
        }
        return result;
    }

    @SuppressWarnings("unchecked")
    public static Map<String, Object> getHttpResponseHeaders(Object port) {
        final Map<String, Object> responseContext = getResponseContext(port);
        if (responseContext == null) return null;
        return (Map<String, Object>) responseContext.get(MessageContext.HTTP_RESPONSE_HEADERS);
    }

    public static Map<String, Object> getResponseContext(Object port) {
        BindingProvider bindingProvider = getBindingProvider(port);
        if (bindingProvider == null) return null;
        return bindingProvider.getResponseContext();
    }

    public static BindingProvider getBindingProvider(Object port) {
        if (port == null) return null;
        return (BindingProvider) port;
    }

}
