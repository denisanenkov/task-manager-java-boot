package ru.anenkov.tm.listener.system;

import org.jetbrains.annotations.NotNull;
import org.springframework.context.event.EventListener;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;
import ru.anenkov.tm.event.ConsoleEvent;
import ru.anenkov.tm.listener.AbstractListenerClient;

@Component
public class AboutDevClientListener extends AbstractListenerClient {

    @NotNull
    @Override
    public String arg() {
        return "-a";
    }

    @NotNull
    @Override
    public String command() {
        return "About";
    }

    @NotNull
    @Override
    public String description() {
        return "Show developer info.";
    }

    @Async
    @Override
    @EventListener(condition = "@aboutDevClientListener.command() == #event.name")
    public void handler(@NotNull final ConsoleEvent event) throws Exception {
        System.out.println("[ABOUT]");
        System.out.println("NAME: Denis Anenkov");
        System.out.println("E-MAIL: denk.an@inbox.ru");
    }

}
