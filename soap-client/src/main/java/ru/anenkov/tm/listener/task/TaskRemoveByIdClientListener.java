package ru.anenkov.tm.listener.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;
import ru.anenkov.tm.endpoint.soap.AuthEndpoint;
import ru.anenkov.tm.endpoint.soap.ProjectDTO;
import ru.anenkov.tm.endpoint.soap.ProjectEndpoint;
import ru.anenkov.tm.endpoint.soap.TaskEndpoint;
import ru.anenkov.tm.event.ConsoleEvent;
import ru.anenkov.tm.listener.AbstractListenerClient;
import ru.anenkov.tm.util.TerminalUtil;

import javax.xml.ws.soap.SOAPFaultException;
import java.util.List;

import static ru.anenkov.tm.listener.AbstractClient.getListSetCookieRow;
import static ru.anenkov.tm.listener.AbstractClient.setMaintain;

@Component
public class TaskRemoveByIdClientListener extends AbstractListenerClient {

    @NotNull
    @Autowired
    private ProjectEndpoint projectEndpoint;

    @NotNull
    @Autowired
    private AuthEndpoint authEndpoint;

    @NotNull
    @Autowired
    private TaskEndpoint taskEndpoint;

    @Override
    public @Nullable String arg() {
        return null;
    }

    @Override
    public @Nullable String command() {
        return "Task-remove-by-id";
    }

    @Override
    public @Nullable String description() {
        return "Task remove by id";
    }

    @Async
    @Override
    @EventListener(condition = "@taskRemoveByIdClientListener.command() == #event.name")
    public void handler(@NotNull final ConsoleEvent event) throws Exception {
        setMaintain(authEndpoint);
        final List<String> session = getListSetCookieRow(authEndpoint);
        System.out.print("ENTER NAME OF PROJECT TO REMOVE TASK: ");
        String name = TerminalUtil.nextLine();
        ProjectDTO projectDTO;
        try {
            projectDTO = projectEndpoint.findProjectByUserIdAndName
                    (name);
        } catch (SOAPFaultException ex) {
            System.out.println("NOT FOUND PROJECT! CHECK THE ENTERED DATA");
            return;
        }
        System.out.print("ENTER ID OF TASK: ");
        String id = TerminalUtil.nextLine();
        try {
            taskEndpoint.removeTaskByUserIdAndProjectIdAndId
                    (projectDTO.getId(), id);
        } catch (SOAPFaultException ex) {
            System.out.println("NOT FOUND! CHECK THE ENTERED DATA");
            return;
        }
    }

}
