package ru.anenkov.tm.listener.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;
import ru.anenkov.tm.endpoint.soap.AuthEndpoint;
import ru.anenkov.tm.endpoint.soap.ProjectEndpoint;
import ru.anenkov.tm.event.ConsoleEvent;
import ru.anenkov.tm.listener.AbstractListenerClient;
import ru.anenkov.tm.util.TerminalUtil;

import java.util.List;

import static ru.anenkov.tm.listener.AbstractClient.*;

@Component
public class ProjectRemoveByIdClientListener extends AbstractListenerClient {

    @NotNull
    @Autowired
    private ProjectEndpoint projectEndpoint;

    @NotNull
    @Autowired
    private AuthEndpoint authEndpoint;

    @Override
    public @Nullable String arg() {
        return null;
    }

    @Override
    public @Nullable String command() {
        return "Project-remove-by-id";
    }

    @Override
    public @Nullable String description() {
        return "Project remove by id";
    }

    @Async
    @Override
    @EventListener(condition = "@projectRemoveByIdClientListener.command() == #event.name")
    public void handler(@NotNull final ConsoleEvent event) throws Exception {
        setMaintain(authEndpoint);
        final List<String> session = getListSetCookieRow(authEndpoint);
        setMaintain(projectEndpoint);
        setListCookieRowRequest(projectEndpoint, session);
        long beginCount = projectEndpoint.count();
        System.out.print("ENTER ID OF PROJECT: ");
        String id = TerminalUtil.nextLine();
        projectEndpoint.removeOneById(id);
        long endCount = projectEndpoint.count();
        if (beginCount - endCount == 1) System.out.println("DELETE PROJECT WITH ID " + id + " SUCCESS");
    }

}
