package ru.anenkov.tm.listener.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;
import ru.anenkov.tm.endpoint.soap.AuthEndpoint;
import ru.anenkov.tm.endpoint.soap.ProjectDTO;
import ru.anenkov.tm.endpoint.soap.ProjectEndpoint;
import ru.anenkov.tm.event.ConsoleEvent;
import ru.anenkov.tm.listener.AbstractListenerClient;
import ru.anenkov.tm.util.TerminalUtil;

import java.util.List;

import static ru.anenkov.tm.listener.AbstractClient.*;

@Component
public class ProjectCreateClientListener extends AbstractListenerClient {

    @NotNull
    @Autowired
    private ProjectEndpoint projectEndpoint;

    @NotNull
    @Autowired
    private AuthEndpoint authEndpoint;

    @Override
    public @Nullable String arg() {
        return null;
    }

    @Override
    public @Nullable String command() {
        return "Project-create";
    }

    @Override
    public @Nullable String description() {
        return "Create project";
    }

    @Async
    @Override
    @EventListener(condition = "@projectCreateClientListener.command() == #event.name")
    public void handler(@NotNull final ConsoleEvent event) throws Exception {
        setMaintain(projectEndpoint);
        final List<String> session = getListSetCookieRow(authEndpoint);
        setMaintain(projectEndpoint);
        setListCookieRowRequest(projectEndpoint, session);
        System.out.print("ENTER NAME: ");
        String name = TerminalUtil.nextLine();
        System.out.print("ENTER DESCRIPTION: ");
        String description = TerminalUtil.nextLine();
        ProjectDTO projectDTO = new ProjectDTO();
        projectDTO.setName(name);
        projectDTO.setDescription(description);
        projectDTO.setUserId(authEndpoint.profile().getId());
        long beginCount = projectEndpoint.count();
        projectEndpoint.addProject(projectDTO);
        long endCount = projectEndpoint.count();
        if (endCount - beginCount == 1) System.out.println("ADD PROJECT SUCCESS");
    }

}
