package ru.anenkov.tm.listener.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;
import ru.anenkov.tm.endpoint.soap.AuthEndpoint;
import ru.anenkov.tm.endpoint.soap.ProjectEndpoint;
import ru.anenkov.tm.event.ConsoleEvent;
import ru.anenkov.tm.listener.AbstractListenerClient;
import ru.anenkov.tm.util.TerminalUtil;

import javax.xml.ws.soap.SOAPFaultException;
import java.util.List;

import static ru.anenkov.tm.listener.AbstractClient.*;

@Component
public class ProjectFindByNameClientListener extends AbstractListenerClient {

    @NotNull
    @Autowired
    private ProjectEndpoint projectEndpoint;

    @NotNull
    @Autowired
    private AuthEndpoint authEndpoint;

    @Override
    public @Nullable String arg() {
        return null;
    }

    @Override
    public @Nullable String command() {
        return "Show-project-by-name";
    }

    @Override
    public @Nullable String description() {
        return "Show project By Name";
    }

    @Async
    @Override
    @EventListener(condition = "@projectFindByNameClientListener.command() == #event.name")
    public void handler(@NotNull final ConsoleEvent event) throws Exception {
        setMaintain(authEndpoint);
        final List<String> session = getListSetCookieRow(authEndpoint);
        setMaintain(projectEndpoint);
        setListCookieRowRequest(projectEndpoint, session);
        System.out.print("ENTER NAME OF PROJECT: ");
        String name = TerminalUtil.nextLine();
        try {
            System.out.println(projectEndpoint.findProjectByUserIdAndName(name).getName() + " " +
                    projectEndpoint.findProjectByUserIdAndName(name).getDescription());
        } catch (SOAPFaultException ex) {
            System.out.println("NOT FOUND TASK BY ENTERED NAME! CHECK THE ENTERED DATA");
            authEndpoint.logout();
            return;
        }
    }

}

