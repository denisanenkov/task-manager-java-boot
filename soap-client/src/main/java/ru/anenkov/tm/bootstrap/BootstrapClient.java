package ru.anenkov.tm.bootstrap;

import lombok.Getter;
import lombok.Setter;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import ru.anenkov.tm.event.ConsoleEvent;
import ru.anenkov.tm.exception.IncorrectCommandException;
import ru.anenkov.tm.listener.AbstractClient;
import ru.anenkov.tm.listener.AbstractListenerClient;
import ru.anenkov.tm.util.TerminalUtil;

import java.util.LinkedHashMap;
import java.util.Map;

@Getter
@Setter
@Component
@Scope("singleton")
public class BootstrapClient {

    @NotNull
    @Autowired
    private ApplicationEventPublisher publisher;

    @NotNull
    @Autowired
    private AbstractListenerClient[] commandClientList;

    @NotNull
    @Autowired
    private AbstractClient abstractClient;

    @NotNull
    public final Map<String, AbstractListenerClient> commands = new LinkedHashMap<>();

    private void initCommands(@NotNull final AbstractListenerClient[] abstractCommandClients) {
        for (@NotNull final AbstractListenerClient abstractListenerClient : abstractCommandClients) {
            commands.put(abstractListenerClient.command(), abstractListenerClient);
        }
    }

    public void run(@Nullable final String[] args) throws Exception {
        initCommands(commandClientList);
        System.out.println("*** WELCOME TO TASK MANAGER ***");
        if (args.length != 0) System.exit(0);
        while (true) {
            try {
                parseCommand(TerminalUtil.nextLine());
            } catch (Exception e) {
                System.err.println(e.getMessage());
                System.out.println("[FAIL]");
            }
        }
    }

    @SneakyThrows
    private void parseCommand(@Nullable final String cmd) {
        if (cmd == null || cmd.isEmpty()) return;
        @Nullable final AbstractListenerClient command = commands.get(cmd);
        if (command == null) throw new IncorrectCommandException(cmd);
        @NotNull final String nameCommand = command.getCommand();
        publisher.publishEvent(new ConsoleEvent(nameCommand));
    }

}
