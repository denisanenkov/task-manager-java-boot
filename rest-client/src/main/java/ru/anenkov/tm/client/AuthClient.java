package ru.anenkov.tm.client;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import ru.anenkov.tm.communication.AuthCommunication;
import ru.anenkov.tm.configuration.MyConfiguration;
import ru.anenkov.tm.entiity.User;
import ru.anenkov.tm.util.TerminalUtil;

import java.util.List;

public class AuthClient {

    private static ApplicationContext context = new AnnotationConfigApplicationContext(MyConfiguration.class);
    private static AuthCommunication communication = context.getBean("userCommunication", AuthCommunication.class);

    public static void main(final String[] args) {
        AuthClient client = new AuthClient();
        client.addUser();
    }

    public List<User> getUserList() {
        List<User> users = communication.users();
        System.out.println(users);
        return users;
    }

    public User getUserById() {
        String id = "";
        System.out.print("ENTER USER ID: ");
        id = TerminalUtil.nextLine();
        User user = communication.getUserById(id);
        System.out.println(user);
        return user;
    }

    public void addUser() {
        User firstUser = new User("first some name", "some description");
        User secondUser = new User("second some name", "some description");
        secondUser.setId(firstUser.getId());
        communication.addUser(firstUser);
        communication.addUser(secondUser);
    }

    public void deleteUser() {
        System.out.print("ENTER USER ID: ");
        String id = TerminalUtil.nextLine();
        communication.deleteUserById(id);
    }

}
